# param1: path to the 'processGivenVideo.py' script
# param2: path to the Fiji binary in the local Fiji installation

python3 $1 $2 outputs SOMEVIDEO 10-12 input_traMarkers/01_GT  inputA/01_RES inputB/01_RES inputC/01_RES


# ~/Apps/Fiji.app/Contents/MacOS/ImageJ-macosx --headless --run "Annotations Merging Tool"  "mergeModel=\"Threshold - flat weights\",filePath=\"/Users/ulman/job_spec.txt\",mergeThreshold=1,fileIdxFrom=1,fileIdxTo=3,outputPath=\"outFolder/maskXXX.tif\"" > log1.txt
