#!/bin/bash

maskFolder=$1

ls $maskFolder/man_track*tif | sort | while read img; do
	echo
	echo
	echo "# $img"
	justImg=`basename $img`
	tp=${justImg:9:3}
	of_histogram $img | grep -Pv '\t0$'  | tail -n +8 | while read ss; do
		echo -e "${tp}\t$ss"
	done
done
