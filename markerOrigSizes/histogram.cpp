#include <iostream>
#include <map>

int main(void)
{
	std::map<int,int> hist;
	int size, bin;
	const int binSize = 50;

	while (std::cin >> size)
	{
		bin = size/binSize;
		hist[bin*binSize]++;
		//std::cout << "DEBUG: " << size << " -> " << bin << "\n";
	}

	for (auto& h : hist)
		std::cout << h.first << "\t" << h.second << "\n";

	return 0;
}
