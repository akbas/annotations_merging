#!/bin/bash
#PBS -A OPEN-19-3
#PBS -q qfree
#PBS -l select=1:ncpus=24,walltime=5:30:00
#PBS -N Fluo-N2DH-SIM+-02__job2

# setup env
module add Python/3.5.2
module add parallel
echo "starting @ "`date`

# prepare input folder
cd /tmp; unzip /scratch/temp/silverGT/sim02_startingPackage.zip > /dev/null
echo "package unzipping done @ "`date`

# prepare tasks folder, and stay in this folder
cd /ramdisk/$PBS_JOBID; unzip /scratch/temp/silverGT/in/Fluo-N2DH-SIM+-02__job2.zip > /dev/null
echo "jobs unzipping done @ "`date`

# look what needs to be computed, and compute it...
rm -f tasklist notComplete
ls -d mer* > tasklist
parallel -a tasklist /tmp/silverGT_ulman/jobParallelProcessOneMerge.sh
echo "parallel done @ "`date`
test -e notComplete && echo "this job is not complete" || echo "this job is complete"

# collect results (and suppress listing of visited files)
zip -r /tmp/Fluo-N2DH-SIM+-02__job2.zip .  > /dev/null
mv /tmp/Fluo-N2DH-SIM+-02__job2.zip /scratch/temp/silverGT/out/Fluo-N2DH-SIM+-02__job2.zip
echo "jobs zipping and moving done @ "`date`

# clean up the /tmp
rm -r /tmp/silverGT_ulman
echo "clean up done; stopping @ "`date`
