import os
import subprocess
import sys

#----------------------------------------------------
# the main script starts here:
if(len(sys.argv.__str__().split()) < 8):	# First parameter calls this function
	sys.exit("There should be at least 7 parameters:\n"
	        +"1. Path Fiji binary\n"
	        +"2. Output Folder\n"
	        +"3. Video Name\n"
	        +"4. Time Points list\n"
	        +"5. TRA Folder\n"
	        +"6. N, Number of fixed inputs >= 0\n"
	        +"7. Input Fixed Folder 1\n"
	        +"..."
	        +"7+N Input Fixed Folder N\n"
	        +"7+N+1 Input Folder N+1\n"
	        +"7+N+2 Input Folder N+2\n"
	        +"...")

inputFolders=[]
for i in range(7,len(sys.argv)):
   inputFolders.append(sys.argv[i])

#print(["INPUTS here: ",inputFolders])
# list of input folders with participants' segmentations
#inputFolders = ["C:/testing/01_RES-01",
#                "C:/testing/01_RES-14"]
fijiBinary = sys.argv[1]
outputFolder = sys.argv[2]	# into this folder, all outputs should be generated
videoname = sys.argv[3]
timesStr = sys.argv[4]
gtFolder = sys.argv[5]		# folder with tracking markers

fixedInputsN = int(sys.argv[6])
inputsIncrement = 2**fixedInputsN

N = len(inputFolders)
#N=0 -- 000000000001 == 1 << 0
#N=1 -- 000000000010 == 1 << 1
#N=2 -- 000000000100 == 1 << 2
#N=3 -- 000000001000 == 1 << 3
#N=4 -- 000000010000 == 1 << 4

# make sure the output folders exist
videoOutFolder = outputFolder + "/" + videoname

outFolderSuffix  = "__job" # +str(outFolderCurIdx)
#
outFolderCurSize = 0
outFolderMaxSize = 3000
#
outFolderCurIdx  = 1
outFolderCurName = videoOutFolder + outFolderSuffix + str(outFolderCurIdx)
if(not os.path.isdir(outFolderCurName)):
	os.makedirs(outFolderCurName)

def zipAndRemoveFolder(folder):
	# get the basename of the folder (and care for trailing '/')
	# (head,bFolder) = os.path.split(folder)
	# if bFolder == '':
	#	(head,bFolder) = os.path.split(head)

	# but since the arg is always outFolderCurName, this never ends with trailing '/'
	bFolder = os.path.basename(folder)

	# zip the current folder and remove it
	cmdFinalize = "cd " + folder + "; zip -r ../" + bFolder +".zip . > /dev/null; cd ..; rm -r " + bFolder
	subprocess.run([cmdFinalize], shell=True,stdout=subprocess.PIPE,universal_newlines=True)

# the combinations sampling happens here
# the current combination is stored in the array of paths: inFolders
for j in range(inputsIncrement-1, 2**N, inputsIncrement):		# examine combination, bit by bit from the right
	inFolders = []
	for i in range(0,N):
		if (j & (1 << i)) > 0:	# if i-th bit is 1, then add inputFolders[i]
			inFolders.append(inputFolders[i])

	Nfol = len(inFolders)
	# this goes over all possible thresholds given there is N input segmentations
	for T in range(1, Nfol+1):
		# update outFolderCurName if necessary
		if (outFolderCurSize >= outFolderMaxSize):
			#
			zipAndRemoveFolder(outFolderCurName)
			#
			outFolderCurSize = 0
			#
			outFolderCurIdx = outFolderCurIdx+1
			outFolderCurName = videoOutFolder + outFolderSuffix + str(outFolderCurIdx)
			if(not os.path.isdir(outFolderCurName)):
				os.makedirs(outFolderCurName)

		# non-colliding name of the output folder + create it
		outFolder = outFolderCurName + "/merged_" + videoname + "_" + str(j).zfill(8) + "_T=" + str(T)
		if(not os.path.isdir(outFolder)):
			os.makedirs(outFolder)

		# create the job.txt in this folder
		jobFile = open(outFolder+"/job.txt","w")
		# add the current input folders into the job.txt
		for I in inFolders:
			jobFile.write(I + "/maskXXX.tif\n")

		# also add the path to the tracking markers into the job.txt + close the file
		jobFile.write(gtFolder + "/TRA/man_trackXXX.tif")
		jobFile.close()

		#print("Prepared " + outFolder)

		cmdSeparator = "echo ============================================= COMMANDENDS ===============================================; echo;"

		cmdFuse = fijiBinary+" --headless --run \"Annotations Merging Tool\" "+\
		          "\"mergeModel=\\\"Threshold - flat weights\\\",filePath=\\\"$1/job.txt\\\",mergeThreshold="\
		          + str(T) + ",fileIdxStr=\\\"" + timesStr + "\\\",outputPath=\\\"$1/maskXXX.tif\\\"\""

		cmdPerf = fijiBinary+" --headless --run \"Cell Tracking Challenge SEG measure\" "+\
		          "\"resPath=\\\"$1\\\",gtPath=\\\"" + gtFolder\
		          + "\\\",fileIdxStr=\\\"" + timesStr + "\\\",optionVerboseLogging=true,optionReportAllResultLabels=false\""

		#cmdCompressTiffs = "for i in "+outFolder+"/mask*tif; do /scratch/work/project/open-15-12/silverGT/scripts/of_changeformat $i $i; done"

		# log the commands used and reports obtained
		logFile = open(outFolder+"/log_1_commands.txt","w")
		logFile.write(cmdFuse)
		logFile.write("\n")
		logFile.write(cmdSeparator)
		logFile.write("\n")
		logFile.write(cmdPerf)
		logFile.write("\n")
		#logFile.write(cmdSeparator)
		#logFile.write("\n")
		#logFile.write(cmdCompressTiffs)
		#logFile.write("\n")
		logFile.close()

		outFolderCurSize = outFolderCurSize+1

zipAndRemoveFolder(outFolderCurName)
