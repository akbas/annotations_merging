# give it a starting job index

jobIDbase=$1
jobVideo="Fluo-N2DL-HeLa-01__job"

jobFile=jobParallelScript_JobArray_${jobIDbase}.sh

echo "#!/bin/bash"                                                                    >  $jobFile
echo "#PBS -A OPEN-15-12"                                                             >> $jobFile
echo "#PBS -q qfree"                                                                  >> $jobFile
echo "#PBS -l select=1:ncpus=24,walltime=12:00:00"                                    >> $jobFile
echo "#PBS -N ${jobVideo}Base${jobIDbase}"                                            >> $jobFile
echo ""                                                                               >> $jobFile
echo "# define the current job"                                                       >> $jobFile
echo "jobID=\$(($jobIDbase+PBS_ARRAY_INDEX))"                                         >> $jobFile
echo "jobName=\"${jobVideo}\$jobID\""                                                 >> $jobFile
echo "zipFile=\"\${jobName}.zip\""                                                    >> $jobFile
echo ""                                                                               >> $jobFile
echo "# start at all?"                                                                >> $jobFile
echo "test -e /scratch/temp/silverGT/out/\$zipFile && exit 0"                         >> $jobFile
echo ""                                                                               >> $jobFile
echo "# setup env"                                                                    >> $jobFile
echo "module add Python/3.5.2"                                                        >> $jobFile
echo "module add parallel"                                                            >> $jobFile
echo 'echo "starting @ "`date`'                                                       >> $jobFile
echo ""                                                                               >> $jobFile
echo "# prepare input folder"                                                         >> $jobFile
echo "cd /tmp; unzip /scratch/temp/silverGT/hela01_startingPackage.zip > /dev/null"   >> $jobFile
echo 'echo "package unzipping done @ "`date`'                                         >> $jobFile
echo ""                                                                               >> $jobFile
echo "# prepare tasks folder, and stay in this folder"                                >> $jobFile
echo "cd /ramdisk/\$PBS_JOBID; unzip /scratch/temp/silverGT/in/\$zipFile > /dev/null" >> $jobFile
echo 'echo "jobs unzipping done @ "`date`'                                            >> $jobFile
echo ""                                                                               >> $jobFile
echo "# look what needs to be computed, and compute it..."                            >> $jobFile
echo "rm -f tasklist notComplete"                                                     >> $jobFile
echo "ls -d mer* > tasklist"                                                          >> $jobFile
echo "parallel -a tasklist /tmp/silverGT_ulman/jobParallelProcessOneMerge.sh"         >> $jobFile
echo 'echo "parallel done @ "`date`'                                                  >> $jobFile
echo "test -e notComplete && echo \"this job is not complete\" || echo \"this job is complete\""  >> $jobFile
echo ""                                                                               >> $jobFile
echo "# collect results (and suppress listing of visited files)"                      >> $jobFile
echo "zip -r /tmp/\$zipFile .  > /dev/null"                                           >> $jobFile
echo "mv /tmp/\$zipFile /scratch/temp/silverGT/out/\$zipFile"                         >> $jobFile
echo 'echo "jobs zipping and moving done @ "`date`'                                   >> $jobFile
echo ""                                                                               >> $jobFile
echo "# clean up the /tmp"                                                            >> $jobFile
echo "rm -r /tmp/silverGT_ulman"                                                      >> $jobFile
echo 'echo "clean up done; stopping @ "`date`'                                        >> $jobFile

#qsub $jobFile
