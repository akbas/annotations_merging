# give it a file from taskLists folder (e.g. file, or can be also taskLists/file)
# this is essentially a nickname of the parallelJob to be executed on one node

inFile=`basename $1`
outFile=/temp/silverGT/runs/${inFile}.sh

echo "#!/bin/bash" > $outFile
echo "#PBS -A OPEN-19-3"  >> $outFile
echo "#PBS -q qfree"  >> $outFile
echo "#PBS -l select=1:ncpus=24,walltime=3:00:00"   >> $outFile
echo "#PBS -N $inFile"   >> $outFile
echo ""  >> $outFile
echo module add Python/3.5.2  >> $outFile
echo module add parallel  >> $outFile
echo 'cd /ramdisk/$PBS_JOBID'  >> $outFile
echo parallel -a /home/xulman/scripts2/taskLists-SIM/$inFile /home/xulman/scripts2/jobParallelProcessOneMerge.sh  >> $outFile
