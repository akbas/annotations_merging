# can be executed from anywhere
videoPartFolderPattern="$1"

cd /temp/silverGT
for v in ${videoPartFolderPattern}; do
	taskListFile=/temp/silverGT/taskLists/`basename $v`
	ls -d $v/merge* > $taskListFile
	/home/ulman/data/silverGT_SIMdata/scripts2/jobParallelScriptGenerator.sh $taskListFile
done
cd -
