python3 \
/home/ulman/data/silverGT_SIMdata/scripts2/prepareGivenVideo_intoParts_zipped.py \
"$1/Fiji.app/ImageJ-linux64" "$2" \
Fluo-N2DH-SIM+-01 \
0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,64 \
"$1/gtReference/Fluo-N2DH-SIM+/01_GT" \
3 \
"$1/userResults/Fluo-N2DH-SIM+/fixed/BGU-IL_(2)/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/fixed/CVUT-CZ/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/fixed/MU-Lux-CZ/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/BGU-IL_(3)/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/BGU-IL_(4)/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/FR-Ro-GE/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/HD-Hau-GE/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/HD-Wol-GE/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/HIT-CN/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/KTH-SE_(1)/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/LEID-NL/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/PAST-FR/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/TUG-AT/01_RES" \
"$1/userResults/Fluo-N2DH-SIM+/UZH-CH/01_RES"
