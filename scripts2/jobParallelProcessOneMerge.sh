#!/bin/bash

# 11hrs and 40 mins is 700 mins
# if current time is 700+ min later than the 'tasklist' file was created, we skip this job
if [ $( find tasklist -cmin +700 | wc -l ) -eq 1 ]; then
	touch "notComplete"
	exit 0
fi

# enter the job folder and execute it
cd $1
if [ ! -e log_2_outputs.txt ]; then
	date                            >  log_2_outputs.txt
	head -n 3 /proc/meminfo         >> log_2_outputs.txt
	source log_1_commands.txt $PWD  >> log_2_outputs.txt 2> /dev/null
	rm mask*.tif
fi
