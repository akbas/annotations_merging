# param1: path to the 'processGivenVideo.py' script
# param2: path to the Fiji binary in the local Fiji installation

#for gtFolder in ../gtReference/Fluo-N3DL-DRO; do
#for gtFolder in ../gtReference/DIC-C2DH-HeLa; do
for gtFolder in ../gtReference/*; do
	for video in 01 02; do
		# readout timepoints that are available in SEG GT
		timePoints=""
		timePointsTRA=""

		# list of available timepoints for the SEG gt
		is2D=`echo $gtFolder | sed 's/.*\([23]D\).*/\1/'`
		if [ $is2D == "2D" ]; then
			for image in $gtFolder/${video}_GT/SEG/*tif; do
				imageTime=`echo $image | sed 's/.*_seg\(...\)\.tif/\1/'`
				timePoints="$timePoints$imageTime,"
			done
		else
			for image in $gtFolder/${video}_GT/SEG/*tif; do
				imageTime=`echo $image | sed 's/.*seg_\(...\)_...\.tif/\1/'`
				timePoints="$timePoints$imageTime,"
			done
		fi
		# the last timepoint is repeated twice
		timePoints="$timePoints$imageTime"

		# list of available timepoints for the TRA gt
		for image in $gtFolder/${video}_GT/TRA/*tif; do
			imageTime=`echo $image | sed 's/.*man_track\(...\)\.tif/\1/'`
			timePointsTRA="$timePointsTRA$imageTime,"
		done
		timePointsTRA="$timePointsTRA$imageTime"

		# intersection of the two lists
		timePointsBoth=`python3 mergeIntervals.py "$timePoints" "$timePointsTRA"`

		if [ -z "$timePointsBoth" ]; then
			echo "$gtFolder/$video PROBLEM: NO INTERSECTING TIMEPOINTS BETWEEN GT-SEG AND GT-TRA"
		else
			gtFolderBasename=`basename $gtFolder`

			# wrap the individual folder names with double quotes (to make it over folder names with spaces or other "crazy" characters)
			fixedFiles=`ls -d ../userResults/$gtFolderBasename/fixed/*/${video}_RES | { while read i; do printf "\"%s\" " "$i"; done }`
			  varFiles=`ls -d ../userResults/$gtFolderBasename/*/${video}_RES       | { while read i; do printf "\"%s\" " "$i"; done }`

			echo python3 $1 $2 ../fusionOutputs "${gtFolderBasename}-$video" "$timePointsBoth" $gtFolder/${video}_GT `ls -d ../userResults/$gtFolderBasename/fixed/*/${video}_RES | wc -l` "$fixedFiles" "$varFiles" > "${gtFolderBasename}-${video}.cmd"
		fi
	done
done
