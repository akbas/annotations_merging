Make sure this folder structure is prepared:

fusionOutputs/
  [initially empty]

gtReference/
  DIC-C2DH-HeLa
  Fluo-C2DL-MSC
  ...
  PhC-C2DL-PSC

userResults/
  anyInputFolderName
  anyAnotherInputFolderName
  ...
  anyYetAnotherInputFolderName
  fixed/anyInputFolderName
  fixed/anyAnotherInputFolderName
  fixed/...
  fixed/anyYetAnotherInputFolderName

binariesAndScripts/
  createScripts.cmd
  removeScripts.cmd

 

Start with:

cd binariesAndScripts
. createScripts.cmd pathTo_processGivenVideo.py pathToFijiBinary
