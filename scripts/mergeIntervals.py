import sys

numbers = sys.argv[1].split(',')
num1 = []
for n in numbers:
	num1.append(int(n))

numbers = sys.argv[2].split(',')
num2 = []
for n in numbers:
	num2.append(int(n))

num = [str(value) for value in num1 if value in num2]

print(",".join(num))
