#!/bin/bash

smallestErosion=1000000

# video 01-test
#for i in `seq 0 4`; do

# video 01
for i in `seq 0 64`; do

# video 02
#for i in `seq 0 149`; do
	echo
	echo "--------------------------------- $i ---------------------------------"

	tp=`printf %03d $i`
	./findMaxDilation  01/silverGT_ulman/gtReference/Fluo-N2DH-SIM+/01_GT/TRA/man_track${tp}.tif  fakeRes.tif
	erosion=$?

	if [ $erosion -lt $smallestErosion ]; then smallestErosion=$erosion; fi
done

echo
echo
echo "Overall smallest erosion is: $smallestErosion"
